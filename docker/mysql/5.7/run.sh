#!/usr/bin/env bash

echo "================================================================================"
echo "= MySQL custom entrypoint                                                      ="
echo "================================================================================"

echo "Autocreate missing directories"
mkdir -p  /var/log/mysql/slow /var/log/mysql/bin

echo "Setup default configuration"
cp -n /etc/my.cnf.dist /etc/mysql/conf.d/my.cnf

echo "Run official entrypoint"
exec /entrypoint.sh "$@"
