#!/bin/bash
# ex: .env
if [ -z "$1" ]
  then
    ENV_FILE=.env
else
  ENV_FILE=$1
fi

diff --color <(grep "=" ${ENV_FILE}.dist | grep -vE "^#" | cut -d "=" -f 1 | sort) <(grep "=" $ENV_FILE | grep -vE "^#" | cut -d "=" -f 1 | sort)

result=$?

if [ $result -ne 0 ]
then
    echo "--------------------------------------------------------------------------------"
    echo "- Your ${ENV_FILE} file is different from ${ENV_FILE}.dist"
    echo "- Red lines: missing definitions in your .env file"
    echo "- Green lines: definitions are not in .env.dist"
    echo "--------------------------------------------------------------------------------"
    exit 1
fi
