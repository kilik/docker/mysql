#!/bin/bash

source .env

# auto create mysql network
docker network inspect ${MYSQL_NETWORK} 2>/dev/null 1>/dev/null
retval="$?"

if [ ! ${retval} -eq 0 ]
then
  echo "creating mysql network '${MYSQL_NETWORK}'"
  docker network create ${MYSQL_NETWORK}
else
  echo "using mysql network '${MYSQL_NETWORK}'"
fi
