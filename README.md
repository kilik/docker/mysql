# MySQL docker image

## Quick setup

Requirements:

- a reverse proxy [kilik proxy](https://gitlab.com/kilik/docker/proxy)


```shell
git clone git@gitlab.com:kilik/docker/mysql.git
cd mysql
make upgrade
```

Default setup:

- [PhpMyadmin http://phpmyadmin.localhost](http://phpmyadmin.localhost)
- mysql network: "mysql"
- port binding: 127.0.0.1:3306

## To build images

Path Dockerfile, then

```shell
./build.sh --path docker/mysql/5.7 --registry kilik/mysql --version 5.7.34 --pull
# append --push to send new tag on registry
```
