.PHONY: help

# User Id
UID = $(shell id -u)
USER = root

## Display this help text
help:
	$(info ---------------------------------------------------------------------)
	$(info -                        Available targets                          -)
	$(info ---------------------------------------------------------------------)
	@awk '/^[a-zA-Z\-\_0-9]+:/ {                                                \
	nb = sub( /^## /, "", helpMsg );                                            \
	if(nb == 0) {                                                               \
		helpMsg = $$0;                                                      \
		nb = sub( /^[^:]*:.* ## /, "", helpMsg );                           \
	}                                                                           \
	if (nb)                                                                     \
		printf "\033[1;31m%-" width "s\033[0m %s\n", $$1, helpMsg;          \
	}                                                                           \
	{ helpMsg = $$0 }'                                                          \
	$(MAKEFILE_LIST) | column -ts:

.env:
	cp .env.dist .env

compare_conf:
	@./scripts/check-env.sh .env || (echo "fix your .env, then try again"; exit 1)

# auto setup
autoconf: .env compare_conf

# auto create networks
create-networks:
	./scripts/create-networks.sh

## Pull images
pull:
	docker-compose pull

## Build images
build: autoconf
	docker-compose build --pull

## Start all the containers
up: autoconf create-networks
	docker-compose up -d
## Alias -> up
start: up

## Stop all the containers
stop:
	docker-compose stop

## Stop, then... start
restart: stop start

## Down all the containers
down:
	docker-compose down --volumes --remove-orphans

## Check stack status
ps:
	docker-compose ps

## Show logs
logs:
	docker-compose logs -tf --tail=1000

## Enter a bash shell
shell:
	docker-compose exec mysql bash

## Enter mysql shell (USER=myuser, default root)
mysql:
	docker-compose exec mysql mysql -h mysql -p -u ${USER}

## Upgrade the stack (pull, then up)
upgrade: pull up
